#!/bin/bash

## USAGE
# gitlab.sh -s SERVERNAME -u USER -d DAILY -w WEEKLY

## DAILY BACKUP ##
# This script does a backup of gitlab from a remote server
# Then it maintans copies of the newest `n` files in each folder.
# The deletion of oldest copies is not based on file creation (date older thax XX days), but on
# minimum number of files that have to be present in backup folder.
# This way it ensures that `n` copies will always be present, although the backup script
# start failing.
# Backups are stored in $mainpath/$servername
# You may wish to set $mainpath as needed
# mainpath="/YOUR/PATH/HERE"
# |
# |__$mainpath/$servername/daily
#               |__/files
#
# |
# |__$mainpath/$servername/weekly
#               |__/files
#
# It's better to store copies in different folders, as this script
# will delete older copies, leaving only newest `n` ones
# If we put in same directory and ddbb backup fails
# we could loose all previous ddb copies
# This way we ensure that there will be at least `n` copies of each (ddb & files)
# and we can set different policies for each of them

##Conf
servername=""
#Leave this files in daily backups folders
daily=4
#Leave this files in weekly backups folders
weekly=4

##Get arguments with getopts
# todo: args validation
while getopts ":p:s:u:d:w:" options; do
  case "${options}" in
    p)
      port=${OPTARG}
      ;;
    s)
      servername=${OPTARG}
      ;;
    u)
      user=${OPTARG}
      ;;
    d)
      daily=${OPTARG}
      ;;
    w)
      weekly=${OPTARG}
      ;;
    :)
      exit 1
      ;;
    *)
      exit 1
      ;;
  esac
done
##Exit script if servername is not present
if [ "$servername" = "" ]; then
  exit 1
fi
##Exit script if user is not present
if [ "$user" = "" ]; then
  exit 1
fi
##If port not present use default 22
if [ "$port" = "" ]; then
  port=22
fi

#Backup dirs
## If backup directories do not exists , let's create them
#Change this with the path you want the backups to be stores
# mainpath="/YOUR/PATH/HERE"
mainpath="/var/maadix/backups-gitlab"
if [[ ! -d "$mainpath/$servername/daily/files" ]]; then
  mkdir -p $mainpath/$servername/daily/files
fi

#Directory for weekly backups
if [[ ! -d "$mainpath/$servername/weekly/files" ]]; then
  mkdir -p $mainpath/$servername/weekly/files
fi

#daily files folder
bfolderfiles="$mainpath/$servername/daily/files"
#weekly files folder
bfolderweeklyfiles="$mainpath/$servername/weekly/files"
#daily conf files folder
bconffolderfiles="$mainpath/$servername/config_backup"

#Backup gitlab backup folder
rsync -a -e "ssh -q -p $port" --delete --rsync-path="sudo rsync" $user@$servername:/var/opt/gitlab/backups $mainpath/$servername/
#Backup gitlab config_backup folder
rsync -a -e "ssh -q -p $port" --delete --rsync-path="sudo rsync" $user@$servername:/etc/gitlab/config_backup $mainpath/$servername/

filename=$servername-$(date +%Y-%m-%d_%H-%M)_gitlab_backup.tar
cd $mainpath/$servername
tar -cf $bfolderfiles/$filename backups

## WEEKLY BACKUP ##

DAYOFWEEK=$(date +"%u")
if [ "$DAYOFWEEK" -eq 6 ];
then
  #Backup vmail
  cp -Rp $bfolderfiles/$filename $bfolderweeklyfiles
fi

# Change head -n -XX value to increase or decrease the number of copy you want to save
#Delete backups daily leaving XX newest copies
if [ -d "$bfolderfiles" ]; then
  count=$(find $bfolderfiles -type f | wc -l)
  if [ $count -gt $daily ]; then
    deletedailyfiles=$(find $bfolderfiles -type f -printf "%T@ $bfolderfiles/%f\n" | sort -n |  awk '{ print $2 }' | head -n -$daily | xargs rm ) 
  fi
fi
#Delete backups weekly leaving XX newest copies 
if [ -d "$bfolderweeklyfiles" ]; then
  count=$(find $bfolderweeklyfiles -type f | wc -l)
  if [ $count -gt $weekly ]; then
    deletefilesweek=$(find $bfolderweeklyfiles -type f -printf "%T@ $bfolderweeklyfiles/%f\n" | sort -n | awk '{ print $2 }' | head -n -$weekly | xargs rm )
  fi
fi

#Delete config_backup daily leaving 60 newest copies
if [ -d "$bconffolderfiles" ]; then
  count=$(find $bconffolderfiles -type f | wc -l)
  if [ $count -gt 60 ]; then
    deletedailyfiles=$(find $bconffolderfiles -type f -printf "%T@ $bconffolderfiles/%f\n" | sort -n |  awk '{ print $2 }' | head -n -60 | xargs rm ) 
  fi
fi
