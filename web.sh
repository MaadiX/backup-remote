#!/bin/bash

## USAGE
# web.sh -s SERVERNAME -u USER -d DAILY -w WEEKLY -m yes|no -l yes|no

## DAILY BACKUP ##
# This script does a backup of all the databases and all the files from /var/www of a remote server
# Then it maintans copies of the newest `n` files in each folder.
# The deletion of oldest copies is not based on file creation (date older thax XX days), but on
# minimum number of files that have to be present in backup folder.
# This way it ensures that `n` copies will always be present, although the backup script
# start failing.
# Backups are stored in $mainpath/$servername
# You may wish to set $mainpath as needed
# mainpath="/YOUR/PATH/HERE"
# |
# |__$mainpath/$servername/daily
#               |__/ddbb
#               |__/files
#
# |
# |__$mainpath/$servername/weekly
#               |__/ddbb
#               |__/files
#
# It's better to store copies in different folders, as this script
# will delete older copies, leaving only newest `n` ones
# If we put in same directory and ddbb backup fails
# we could loose all previous ddb copies
# This way we ensure that there will be at least `n` copies of each (ddb & files)
# and we can set different policies for each of them

##Conf
servername=""
#Leave this files in daily backups folders
daily=40
#Leave this files in weekly backups folders
weekly=30

##Get arguments with getopts
# todo: args validation
while getopts ":p:s:u:d:w:m:l:q:" options; do
  case "${options}" in
    p)
      port=${OPTARG}
      ;;
    s)
      servername=${OPTARG}
      ;;
    u)
      user=${OPTARG}
      ;;
    d)
      daily=${OPTARG}
      ;;
    w)
      weekly=${OPTARG}
      ;;
    m)
      mysql=${OPTARG}
      ;;
    l)
      ldap=${OPTARG}
      ;;
    q)
      psql=${OPTARG}
      ;;
    :)
      exit 1
      ;;
    *)
      exit 1
      ;;
  esac
done
##Exit script if servername is not present
if [ "$servername" = "" ]; then
  exit 1
fi
##Exit script if user is not present
if [ "$user" = "" ]; then
  exit 1
fi
##If port not present use default 22
if [ "$port" = "" ]; then
  port=22
fi


#Backup dirs
## If backup directories do not exists , let's create them
#Change this with the path you want the backups to be stores
# mainpath="/YOUR/PATH/HERE"
mainpath="/var/maadix/backups"
if [ "$mysql" == "yes" ]; then
  # directory for daily databases
  if [[ ! -d "$mainpath/$servername/daily/ddbb" ]]; then
    mkdir -p $mainpath/$servername/daily/ddbb
  fi
fi
if [ "$ldap" == "yes" ]; then
  # directory for daily ldap
  if [[ ! -d "$mainpath/$servername/daily/ldap" ]]; then
    mkdir -p $mainpath/$servername/daily/ldap
  fi
fi
if [ "$psql" == "yes" ]; then
  # directory for daily psql
  if [[ ! -d "$mainpath/$servername/daily/psql" ]]; then
    mkdir -p $mainpath/$servername/daily/psql
  fi
fi
# Directory for daily files backup
if [[ ! -d "$mainpath/$servername/daily/files" ]]; then
  mkdir -p $mainpath/$servername/daily/files
fi

if [ "$mysql" == "yes" ]; then
  #Directeory for weekly database backups
  if [[ ! -d "$mainpath/$servername/weekly/ddbb" ]]; then
    mkdir -p $mainpath/$servername/weekly/ddbb
  fi
fi
if [ "$ldap" == "yes" ]; then
  #Directeory for weekly ldap backups
  if [[ ! -d "$mainpath/$servername/weekly/ldap" ]]; then
    mkdir -p $mainpath/$servername/weekly/ldap
  fi
fi
if [ "$psql" == "yes" ]; then
  #Directeory for weekly psql backups
  if [[ ! -d "$mainpath/$servername/weekly/psql" ]]; then
    mkdir -p $mainpath/$servername/weekly/psql
  fi
fi
#Directeory for weekly files backups
if [[ ! -d "$mainpath/$servername/weekly/files" ]]; then
  mkdir -p $mainpath/$servername/weekly/files
fi

#daily databases folder
bfolderddbb="$mainpath/$servername/daily/ddbb"
#daily ldap folder
bfolderldap="$mainpath/$servername/daily/ldap"
#daily psql folder
bfolderpsql="$mainpath/$servername/daily/psql"
#daily files folder
bfolderfiles="$mainpath/$servername/daily/files"
#weekly databases folder
bfolderweeklyddbb="$mainpath/$servername/weekly/ddbb"
#weekly ldap folder
bfolderweeklyldap="$mainpath/$servername/weekly/ldap"
#weekly psql folder
bfolderweeklypsql="$mainpath/$servername/weekly/psql"
#weekly files folder
bfolderweeklyfiles="$mainpath/$servername/weekly/files"

#Backup all mysql ddbb
if [ "$mysql" == "yes" ]; then
  filenamedb=allddbb_$(date +%Y-%m-%d_%H-%M).gz
  ssh -q -p $port -l $user $servername "sudo mysqldump --all-databases | gzip -3 -c" > $bfolderddbb/$filenamedb
fi

#Backup ldap
if [ "$ldap" == "yes" ]; then
  filenameldap=ldap_$(date +%Y-%m-%d_%H-%M).gz
  ssh -q -p $port -l $user $servername "sudo ldapsearch -Q -H ldapi:// -Y EXTERNAL -b 'dc=example,dc=tld' | gzip -3 -c" > $bfolderldap/$filenameldap
fi

#Backup psql
if [ "$psql" == "yes" ]; then
  filenamepsql=psql_$(date +%Y-%m-%d_%H-%M).gz
  ssh -q -p $port -l $user $servername "cd /tmp/; sudo sudo -u postgres pg_dumpall | gzip -3 -c" > $bfolderpsql/$filenamepsql
fi

#Backup /var/www
rsync -a -e "ssh -q -p $port" --delete --rsync-path="sudo rsync" $user@$servername:/var/www $mainpath/$servername/
filename=$servername-$(date +%Y-%m-%d_%H-%M).tar.gz
cd $mainpath/$servername
tar --warning=no-file-ignored -czf $bfolderfiles/$filename www

## WEEKLY BACKUP ##

DAYOFWEEK=$(date +"%u")
if [ "$DAYOFWEEK" -eq 6 ];
then
  if [ "$mysql" == "yes" ]; then
    #Backup all ddbb
    cp -Rp $bfolderddbb/$filenamedb $bfolderweeklyddbb
  fi
  if [ "$ldap" == "yes" ]; then
    #Backup ldap
    cp -Rp $bfolderldap/$filenameldap $bfolderweeklyldap
  fi
  if [ "$psql" == "yes" ]; then
    #Backup psql
    cp -Rp $bfolderpsql/$filenamepsql $bfolderweeklypsql
  fi

  #Backup /var/www
  cp -Rp $bfolderfiles/$filename $bfolderweeklyfiles
fi

# Change head -n -XX value to increase or decrease the number of copy you want to save

#Delete backups daily leaving XX newest copies in database backup folder
if [ "$mysql" == "yes" ]; then
  if [ -d "$bfolderddbb" ]; then
    count=$(find $bfolderddbb -type f | wc -l)
    if [ $count -gt $daily ]; then
      deletedailydb=$(find $bfolderddbb -type f -printf "%T@ $bfolderddbb/%f\n" | sort -n | awk '{ print $2 }' | head -n -$daily | xargs rm )
    fi
  fi
fi

#Delete backups daily leaving XX newest copies in ldap backup folder
if [ "$ldap" == "yes" ]; then
  if [ -d "$bfolderldap" ]; then
    count=$(find $bfolderldap -type f | wc -l)
    if [ $count -gt $daily ]; then
      deletedailydb=$(find $bfolderldap -type f -printf "%T@ $bfolderldap/%f\n" | sort -n | awk '{ print $2 }' | head -n -$daily | xargs rm )
    fi
  fi
fi

#Delete backups daily leaving XX newest copies in psql backup folder
if [ "$psql" == "yes" ]; then
  if [ -d "$bfolderpsql" ]; then
    count=$(find $bfolderpsql -type f | wc -l)
    if [ $count -gt $daily ]; then
      deletedailydb=$(find $bfolderpsql -type f -printf "%T@ $bfolderpsql/%f\n" | sort -n | awk '{ print $2 }' | head -n -$daily | xargs rm )
    fi
  fi
fi

#Delete backups daily leaving XX newest copies in database backup folder
if [ -d "$bfolderfiles" ]; then
  count=$(find $bfolderfiles -type f | wc -l)
  if [ $count -gt $daily ]; then
    deletedailyfiles=$(find $bfolderfiles -type f -printf "%T@ $bfolderfiles/%f\n" | sort -n |  awk '{ print $2 }' | head -n -$daily | xargs rm ) 
  fi
fi

#Delete backups weekly leaving XX newest copies 
if [ "$mysql" == "yes" ]; then
  if [ -d "$bfolderweeklyddbb" ]; then
    count=$(find $bfolderweeklyddbb -type f | wc -l)
    if [ $count -gt $weekly ]; then
      deletedbweek=$(find $bfolderweeklyddbb -type f -printf "%T@ $bfolderweeklyddbb/%f\n" | sort -n | awk '{ print $2 }' | head -n -$weekly | xargs rm )
    fi
  fi
fi

#Delete backups weekly leaving XX newest copies 
if [ "$ldap" == "yes" ]; then
  if [ -d "$bfolderweeklyldap" ]; then
    count=$(find $bfolderweeklyldap -type f | wc -l)
    if [ $count -gt $weekly ]; then
      deletedbweek=$(find $bfolderweeklyldap -type f -printf "%T@ $bfolderweeklyldap/%f\n" | sort -n | awk '{ print $2 }' | head -n -$weekly | xargs rm )
    fi
  fi
fi

#Delete backups weekly leaving XX newest copies 
if [ "$psql" == "yes" ]; then
  if [ -d "$bfolderweeklypsql" ]; then
    count=$(find $bfolderweeklypsql -type f | wc -l)
    if [ $count -gt $weekly ]; then
      deletedbweek=$(find $bfolderweeklypsql -type f -printf "%T@ $bfolderweeklypsql/%f\n" | sort -n | awk '{ print $2 }' | head -n -$weekly | xargs rm )
    fi
  fi
fi

#Delete backups weekly leaving XX newest copies 
if [ -d "$bfolderweeklyfiles" ]; then
  count=$(find $bfolderweeklyfiles -type f | wc -l)
  if [ $count -gt $weekly ]; then
    deletefilesweek=$(find $bfolderweeklyfiles -type f -printf "%T@ $bfolderweeklyfiles/%f\n" | sort -n | awk '{ print $2 }' | head -n -$weekly | xargs rm )
  fi
fi
