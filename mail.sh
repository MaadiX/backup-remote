#!/bin/bash

## USAGE
# mail.sh -s SERVERNAME -u USER -d DAILY -w WEEKLY -l yes|no

## DAILY BACKUP ##
# This script does a backup of all the mails from /home/vmail of a remote server
# Then it maintans copies of the newest `n` files in each folder.
# The deletion of oldest copies is not based on file creation (date older thax XX days), but on
# minimum number of files that have to be present in backup folder.
# This way it ensures that `n` copies will always be present, although the backup script
# start failing.
# Backups are stored in $mainpath/$servername
# You may wish to set $mainpath as needed
# mainpath="/YOUR/PATH/HERE"
# |
# |__$mainpath/$servername/daily
#               |__/files
#
# |
# |__$mainpath/$servername/weekly
#               |__/files
#
# It's better to store copies in different folders, as this script
# will delete older copies, leaving only newest `n` ones
# If we put in same directory and ddbb backup fails
# we could loose all previous ddb copies
# This way we ensure that there will be at least `n` copies of each (ddb & files)
# and we can set different policies for each of them

##Conf
servername=""
#Leave this files in daily backups folders
daily=4
#Leave this files in weekly backups folders
weekly=4

##Get arguments with getopts
# todo: args validation
while getopts ":p:s:u:d:w:l:" options; do
  case "${options}" in
    p)
      port=${OPTARG}
      ;;
    s)
      servername=${OPTARG}
      ;;
    u)
      user=${OPTARG}
      ;;
    d)
      daily=${OPTARG}
      ;;
    w)
      weekly=${OPTARG}
      ;;
    l)
      ldap=${OPTARG}
      ;;
    :)
      exit 1
      ;;
    *)
      exit 1
      ;;
  esac
done


##Exit script if servername is not present
if [ "$servername" = "" ]; then
  exit 1
fi
##Exit script if user is not present
if [ "$user" = "" ]; then
  exit 1
fi
##If port not present use default 22
if [ "$port" = "" ]; then
  port=22
fi

#Backup dirs
## If backup directories do not exists , let's create them
#Change this with the path you want the backups to be stores
# mainpath="/YOUR/PATH/HERE"
mainpath="/var/maadix/backups-mails"

if [[ ! -d "$mainpath/$servername/daily/files" ]]; then
  mkdir -p $mainpath/$servername/daily/files
fi

#Directory for weekly backups
if [[ ! -d "$mainpath/$servername/weekly/files" ]]; then
  mkdir -p $mainpath/$servername/weekly/files
fi

if [ "$ldap" == "yes" ]; then
  # directory for daily ldap
  if [[ ! -d "$mainpath/$servername/daily/ldap" ]]; then
    mkdir -p $mainpath/$servername/daily/ldap
  fi
fi

if [ "$ldap" == "yes" ]; then
  #Directeory for weekly ldap backups
  if [[ ! -d "$mainpath/$servername/weekly/ldap" ]]; then
    mkdir -p $mainpath/$servername/weekly/ldap
  fi
fi


#daily files folder
bfolderfiles="$mainpath/$servername/daily/files"
#weekly files folder
bfolderweeklyfiles="$mainpath/$servername/weekly/files"

#daily ldap folder
bfolderldap="$mainpath/$servername/daily/ldap"
#weekly ldap folder
bfolderweeklyldap="$mainpath/$servername/weekly/ldap"

#Backup mail folder
rsync -a -e "ssh -q -p $port" --delete --rsync-path="sudo rsync" $user@$servername:/home/vmail $mainpath/$servername/

filename=$servername-$(date +%Y-%m-%d_%H-%M).tar.gz
cd $mainpath/$servername
tar -czf $bfolderfiles/$filename vmail

#Backup ldap
if [ "$ldap" == "yes" ]; then
  filenameldap=ldap_$(date +%Y-%m-%d_%H-%M).gz
  ssh -q -p $port -l $user $servername "sudo ldapsearch -Q -H ldapi:// -Y EXTERNAL -b 'dc=example,dc=tld' | gzip -3 -c" > $bfolderldap/$filenameldap
fi

## WEEKLY BACKUP ##

DAYOFWEEK=$(date +"%u")
if [ "$DAYOFWEEK" -eq 6 ];
then
  #Backup vmail
  cp -Rp $bfolderfiles/$filename $bfolderweeklyfiles
fi

if [ "$DAYOFWEEK" -eq 6 ];
then
  if [ "$ldap" == "yes" ]; then
    #Backup ldap
    cp -Rp $bfolderldap/$filenameldap $bfolderweeklyldap
  fi
fi

#Delete backups daily leaving XX newest copies in database backup folder
# Change head -n -XX value to increase or decrease the number of copy you want to save
#Delete backups daily leaving XX newest copies in database backup folder
if [ -d "$bfolderfiles" ]; then
  count=$(find $bfolderfiles -type f | wc -l)
  if [ $count -gt $daily ]; then
    deletedailyfiles=$(find $bfolderfiles -type f -printf "%T@ $bfolderfiles/%f\n" | sort -n |  awk '{ print $2 }' | head -n -$daily | xargs rm ) 
  fi
fi
#Delete backups weekly leaving XX newest copies 
if [ -d "$bfolderweeklyfiles" ]; then
  count=$(find $bfolderweeklyfiles -type f | wc -l)
  if [ $count -gt $weekly ]; then
    deletefilesweek=$(find $bfolderweeklyfiles -type f -printf "%T@ $bfolderweeklyfiles/%f\n" | sort -n | awk '{ print $2 }' | head -n -$weekly | xargs rm )
  fi
fi

#Delete backups daily leaving XX newest copies in ldap backup folder
if [ "$ldap" == "yes" ]; then
  if [ -d "$bfolderldap" ]; then
    count=$(find $bfolderldap -type f | wc -l)
    if [ $count -gt $daily ]; then
      deletedailydb=$(find $bfolderldap -type f -printf "%T@ $bfolderldap/%f\n" | sort -n | awk '{ print $2 }' | head -n -$daily | xargs rm )
    fi
  fi
fi

#Delete backups weekly leaving XX newest copies 
if [ "$ldap" == "yes" ]; then
  if [ -d "$bfolderweeklyldap" ]; then
    count=$(find $bfolderweeklyldap -type f | wc -l)
    if [ $count -gt $weekly ]; then
      deletedbweek=$(find $bfolderweeklyldap -type f -printf "%T@ $bfolderweeklyldap/%f\n" | sort -n | awk '{ print $2 }' | head -n -$weekly | xargs rm )
    fi
  fi
fi
